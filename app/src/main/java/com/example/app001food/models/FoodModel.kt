package com.example.app001food.models

data class FoodModel(
    var id: Int,
    var name: String,
    var image_uri: String,
    var favorites: Int,
    var check: Int = favorites
)