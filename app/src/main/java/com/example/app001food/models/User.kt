package com.example.app001food.models

data class User(
    var id: Int,
    var name: String,
    var email: String,
    var imageUri: String
)