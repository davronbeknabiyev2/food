package com.example.app001food

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.ViewParent
import androidx.viewpager.widget.ViewPager
import com.example.app001food.R
import com.example.app001food.adapters.TabAdapter
import com.example.app001food.models.FoodModel
import com.example.app001food.models.User
import com.example.app001food.ui.foodmenu.FoodMenuContract
import com.example.app001food.ui.foodmenu.FoodMenuModel
import com.example.app001food.ui.foodmenu.FoodMenuPresenter
import com.example.app001food.ui.foodmenu.fragments.ListFoodMenuFragment
import com.google.android.material.tabs.TabLayout
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_food_menu.*

class FoodMenuActivity : AppCompatActivity(), FoodMenuContract.View {
    lateinit var adapter: TabAdapter

    private val fragmentLatest = ListFoodMenuFragment(1)
    private val fragmentFavorites = ListFoodMenuFragment(2)
    lateinit var presenter: FoodMenuContract.Presenter
    private var userId: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_food_menu)

        var intent: Intent = intent

        var a: Int = 5

        this.userId = intent.getIntExtra("userId", -1)
        Log.d("BB", "FoodMenuActivity onCreate: ${this.userId}")
        adapter = TabAdapter(supportFragmentManager)

        adapter.addFragment(fragmentLatest, "Latest")
        adapter.addFragment(fragmentFavorites, "Favorites")
        view_pager_food.adapter = adapter
        tab_layout.setupWithViewPager(view_pager_food)

        view_pager_food.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                Log.d("TTT", "onPageSelected position: $position")
                when (position) {
                    0 -> {
                        fragmentLatest.presenter.init()
                    }
                    1 -> {
                        fragmentFavorites.presenter.init()
                    }
                }
            }
        })

        presenter = FoodMenuPresenter(this, FoodMenuModel(this))
        presenter.inti()

    }

    override fun getUserId() = userId

    override fun setUser(user: User) {
        text_title_food.text = user.name
        Picasso.get()
            .load(user.imageUri)
            .into(image_icon_user)
    }
}
