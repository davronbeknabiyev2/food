package com.example.app001food

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.app001food.models.User
import com.example.app001food.ui.signin.SignInContract
import com.example.app001food.ui.signin.SignInModel
import com.example.app001food.ui.signin.SignInPresenter
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_sign_in.*

class SignInActivity : AppCompatActivity(), SignInContract.View {

    var mGoogleSignInClient: GoogleSignInClient? = null
    lateinit var presenter: SignInContract.Presenter
    private lateinit var task: Task<GoogleSignInAccount>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)


        val gso: GoogleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()


        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        Log.d("BB", "1 SignInActivity onCreate: ")
        presenter = SignInPresenter(this, SignInModel(this))

        button_google.setOnClickListener {
            presenter.signInWithGoogle()
        }
        button_sign_in.setOnClickListener {
            presenter.signIn()
        }

        button_new_account.setOnClickListener {
            //            presenter.createNewAccount()
        }
    }

    override fun startGoogleSignInClient() {
        val intent = mGoogleSignInClient?.signInIntent
        startActivityForResult(intent, 123)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 123) {
            task = GoogleSignIn.getSignedInAccountFromIntent(data)
            presenter.onActivityResult()
        }
    }

    override fun getTaskGoogle(): Task<GoogleSignInAccount> {
        return task
    }

    override fun getName(): String = edit_name.text.toString()

    override fun getEmail(): String = edit_email.text.toString()

    override fun showError(massage: String) {
        Toast.makeText(this, massage, Toast.LENGTH_LONG).show()
    }


    override fun showProgress() {
        progress_layout.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progress_layout.visibility = View.GONE
    }

    override fun successTo(userId: Long) {
        val intent = Intent(this, FoodMenuActivity::class.java)
        intent.putExtra("userId", userId.toInt())
        Log.d("TTT", "userId: $userId")
        startActivity(intent)
        finish()
    }

    override fun successToUser(userId: User) {
        val intent = Intent(this, FoodMenuActivity::class.java)
        intent.putExtra("userId", userId.id.toInt())
//        Log.d("BB", "userId: $userId")
        Log.d("BB", "SignInActivity successToUser: ${userId.id}")
        Log.d("BB", "SignInActivity successToUser: ${userId.email}")
        startActivity(intent)
        finish()
    }
}
