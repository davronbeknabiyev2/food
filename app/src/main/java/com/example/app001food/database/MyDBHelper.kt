package com.example.app001food.database

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.example.app001food.models.FoodModel
import com.example.app001food.models.User

class MyDBHelper(var context: Context) : DBHelper(context, "food.db") {
    var sqLiteDatabase: SQLiteDatabase = this.database()

    fun getFoodList(): List<FoodModel> {
        val list: List<FoodModel> = arrayListOf()
        val c = sqLiteDatabase.rawQuery("select * from food_menu", null)
        if (c.moveToFirst()) {
            do {
                var foodModel = FoodModel(
                    c.getInt(c.getColumnIndex("id")),
                    c.getString(c.getColumnIndex("name")),
                    c.getString(c.getColumnIndex("image_uri")),
                    c.getInt(c.getColumnIndex("favorites"))
                )
                (list as ArrayList).add(foodModel)
            } while (c.moveToNext())
        }
        return list
    }

    fun setUser(user: User): Long {
        val contentValues = ContentValues()
        contentValues.put("name", user.name)
        contentValues.put("email", user.email)
        contentValues.put("image_uri", user.imageUri)
        return sqLiteDatabase.insert("User", null, contentValues)
    }


    fun getUserEmail(email: String): User {
        val user: User
        val cursor = sqLiteDatabase.rawQuery("select * from User where email='$email'", null)
        if (cursor.moveToFirst()) {
            user = User(
                cursor.getInt(cursor.getColumnIndex("id")),
                cursor.getString(cursor.getColumnIndex("name")),
                cursor.getString(cursor.getColumnIndex("email")),
                cursor.getString(cursor.getColumnIndex("image_uri"))
            )
            return user
        }
        return null!!
    }


    fun getUserId(id: Int): User {
        val user: User
        val cursor = sqLiteDatabase.rawQuery("select * from User where id='$id'", null)
        if (cursor.moveToFirst()) {
            user = User(
                cursor.getInt(cursor.getColumnIndex("id")),
                cursor.getString(cursor.getColumnIndex("name")),
                cursor.getString(cursor.getColumnIndex("email")),
                cursor.getString(cursor.getColumnIndex("image_uri"))
            )
            return user
        }
        return null!!
    }

    fun getUserId(name: String, email: String): Int {
        var a: Int = -1
        var c = sqLiteDatabase.rawQuery(
            "select id from User where name=? and email=?",
            listOf(name, email).toTypedArray()
        )
//        Log.d("TTT", c.columnCount.toString())
        if (c.moveToFirst()) {
            a = c.getInt(c.getColumnIndex("id"))
        }
        return a
    }

    fun setFavoritesFood(id: Int) {
        var cursor =
            sqLiteDatabase.rawQuery("select favorites from food_menu where id='$id'", null)
        var fav = -1
        if (cursor.moveToFirst()) {
            fav = cursor.getInt(cursor.getColumnIndex("favorites"))
        }
        Log.d("TTT", "favorites: $fav")
        var contentValues = ContentValues()
        if (fav == 0) {
            contentValues.put("favorites", 1)
        } else {
            contentValues.put("favorites", 0)
        }
        sqLiteDatabase.update("food_menu", contentValues, "id='$id'", null)
    }

}