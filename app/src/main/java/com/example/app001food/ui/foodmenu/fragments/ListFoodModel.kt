package com.example.app001food.ui.foodmenu.fragments

import android.content.Context
import com.example.app001food.database.MyDBHelper
import com.example.app001food.models.FoodModel

class ListFoodModel(context: Context) : ListFoodModelContract.Model {
    var myDBHelper = MyDBHelper(context)

    override fun getLatestList(): List<FoodModel> {
        return myDBHelper.getFoodList()
    }

    override fun getFavoritesList(): List<FoodModel> {
        var list = arrayListOf<FoodModel>()
        myDBHelper.getFoodList().forEach { food ->
            if (food.favorites == 1) {
                list.add(food)
            }
        }
        return list
    }

    override fun checkFavoritesItem(foodId: Int) {
        myDBHelper.setFavoritesFood(foodId)
    }
}