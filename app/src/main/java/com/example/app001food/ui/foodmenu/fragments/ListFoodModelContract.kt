package com.example.app001food.ui.foodmenu.fragments

import com.example.app001food.models.FoodModel

interface ListFoodModelContract {
    interface Model {
        fun getLatestList(): List<FoodModel>
        fun getFavoritesList(): List<FoodModel>
        fun checkFavoritesItem(foodId: Int)
    }

    interface View {
        fun getCheckedFood(): FoodModel
        fun setList(list: List<FoodModel>)
        fun showFavoritesCheck()
        fun showFavoritesUnCheck()
        fun getInitIdFood(): Int
    }

    interface Presenter {
        fun checkFavorites()
        fun init()
    }
}