package com.example.app001food.ui.signin

import android.content.Context
import android.util.Log
import com.example.app001food.database.MyDBHelper
import com.example.app001food.models.User

class SignInModel(var context: Context) : SignInContract.Model {
    var myDBHelper: MyDBHelper = MyDBHelper(context)

    override fun saveModel(s: String): User {
//        val foodList = myDBHelper.getFoodList()
//        foodList.forEach { u ->
//            Log.d("TTT", "Hi: ${u.name}")
//        }
        return myDBHelper.getUserId(1)
    }

    override fun getUserId(name: String, email: String): Long {
        return myDBHelper.getUserId(name, email)
    }

    override fun saveUser(user: User): Long {
        val user1 = myDBHelper.setUser(user)
        return user1
    }

    override fun getUserEmail(email: String): User {
        return myDBHelper.getUserEmail(email)
    }
}
