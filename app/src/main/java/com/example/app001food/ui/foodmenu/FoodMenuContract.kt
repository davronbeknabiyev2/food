package com.example.app001food.ui.foodmenu

import com.example.app001food.models.FoodModel
import com.example.app001food.models.User

interface FoodMenuContract {
    interface Model {
        fun getUser(id: Int): User
    }

    interface View {
        fun setUser(user: User)
        fun getUserId(): Int
    }

    interface Presenter {
        fun checkFavorites()
        fun inti()
    }
}