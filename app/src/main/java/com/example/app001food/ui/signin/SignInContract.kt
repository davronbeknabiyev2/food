package com.example.app001food.ui.signin

import com.example.app001food.models.User
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task

interface SignInContract {
    interface Model {
        fun getUserId(name: String, email: String): Long
        fun saveModel(string: String): User
        fun saveUser(user: User): Long
        fun getUserEmail(email: String): User
    }

    interface View {
        fun getName(): String
        fun getEmail(): String

        fun showError(massage: String)

        fun showProgress()
        fun hideProgress()

        fun successTo(userId: Long)
        fun successToUser(userId: User)

        fun getTaskGoogle(): Task<GoogleSignInAccount>

        fun startGoogleSignInClient()
    }

    interface Presenter {
        fun signIn()
        fun signInWithGoogle()
        fun signInWithFacebook()
        fun createNewAccount()
        fun onActivityResult()
        //N1E9M7A8T
    }
}