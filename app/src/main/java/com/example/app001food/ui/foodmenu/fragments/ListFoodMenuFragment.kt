package com.example.app001food.ui.foodmenu.fragments


import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

import com.example.app001food.R
import com.example.app001food.adapters.FoodMenuAdapter
import com.example.app001food.models.FoodModel

/**
 * A simple [Fragment] subclass.
 */
class ListFoodMenuFragment(var initId: Int) : Fragment(), ListFoodModelContract.View {


    lateinit var recyclerView: RecyclerView
    lateinit var adapter: FoodMenuAdapter
    lateinit var foodModel: FoodModel
    lateinit var presenter: ListFoodModelContract.Presenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_list_food_menu, container, false)
        recyclerView = view.findViewById(R.id.recycler_view_food_fragment)
        recyclerView.layoutManager =
            StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)

        adapter = FoodMenuAdapter() {
            foodModel = it
            Log.d("TTT", foodModel.name)
            presenter.checkFavorites()
        }
        recyclerView.adapter = adapter
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = ListFoodPresenter(this, ListFoodModel(context!!))
        presenter.init()
    }

    override fun setList(list: List<FoodModel>) {
        adapter.setFoodList(list)
        adapter.notifyDataSetChanged()
    }

    override fun getCheckedFood(): FoodModel {
        return foodModel
    }

    override fun showFavoritesCheck() {

    }

    override fun showFavoritesUnCheck() {

    }

    override fun getInitIdFood(): Int = initId
}
