package com.example.app001food.ui.signin

import android.app.Activity
import android.util.Log
import com.example.app001food.models.User
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.ApiException
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.GoogleAuthProvider


class SignInPresenter(var view: SignInContract.View, var model: SignInContract.Model) :
    SignInContract.Presenter {

    private var mAuth: FirebaseAuth = FirebaseAuth.getInstance()

    init {
        Log.d("BB", "SignInPresenter init: ")
        if (mAuth.currentUser != null) {
            val user = mAuth.currentUser
            updateUser2(user!!)
//            Log.d("BB", "SignInPresenter init: ${user.email}")
        }
    }

    override fun signIn() {
        var name = view.getName()
        var email = view.getEmail()

        view.showProgress()
        var userId = model.getUserId(name, email)
        view.hideProgress()
        Log.d("TTT", "" + userId)
        if (userId == (-1).toLong()) {
            view.showError("Login or Password wrong!!!")
            return
        }
        view.successTo(userId)
    }

    override fun signInWithGoogle() {
        view.showProgress()
        view.startGoogleSignInClient()
    }

    override fun signInWithFacebook() {

    }

    override fun createNewAccount() {
        model.saveModel("")
    }

    override fun onActivityResult() {
        var task = view.getTaskGoogle()
        try {
            val account = task.getResult(ApiException::class.java)
            if (account != null) firebaseAuthWithGoogle(account)
        } catch (e: ApiException) {
            Log.d("TTT", "Google sign in failed", e)
        }
    }

    private fun firebaseAuthWithGoogle(account: GoogleSignInAccount) {
        Log.d("TTT", "firebaseAuthWithGoogle:" + account.getId());

        val credential: AuthCredential = GoogleAuthProvider.getCredential(account.idToken, null)
        mAuth?.signInWithCredential(credential)?.addOnCompleteListener(view as Activity) {
            if (it.isSuccessful) {
                view.hideProgress()

                Log.d("TTT", "signInWithCredential:success");

                val user = mAuth?.currentUser

                updateUser(user!!)
            } else {
                view.hideProgress()
                Log.w("TAG", "signInWithCredential:failure")
                updateUser(null!!);
            }
        }
    }

    private fun updateUser(user: FirebaseUser) {
        Log.d("BB", "SignInPresenter updateUser: ${user.email}")
        if (user != null) {
            val name = user.displayName.toString()
            val email = user.email.toString()
            val imageUri = user.photoUrl.toString()

            val userModel = User(
                0,
                name,
                email,
                imageUri
            )
            val saveUser = model.saveUser(userModel)
            view.successTo(saveUser)
        } else {

        }
    }

    private fun updateUser2(user: FirebaseUser) {
        Log.d("BB", "SignInPresenter updateUser2: ${user.email}")
        if (user != null) {
            val email = user.email.toString()
            val saveUser = model.getUserEmail(email)
            Log.d("TTT", "save User: ${saveUser.id}")
            Log.d("TTT", "save User: ${saveUser.name}")
            Log.d("TTT", "save User: ${saveUser.email}")
            Log.d("TTT", "save User: ${saveUser.imageUri}")
            view.successToUser(saveUser)
        }
    }
}