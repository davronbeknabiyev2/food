package com.example.app001food.ui.foodmenu

import android.content.Context
import com.example.app001food.database.MyDBHelper
import com.example.app001food.models.FoodModel
import com.example.app001food.models.User

class FoodMenuModel(context: Context) : FoodMenuContract.Model {
    var myDBHelper = MyDBHelper(context)

    override fun getUser(id: Int): User {
        return myDBHelper.getUserId(id)
    }
}